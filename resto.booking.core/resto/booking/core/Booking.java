package resto.booking.core;

public interface Booking {
	public void book(String name, int partySize, LocalDateTime dateTime);
	public String getBookingList();
}
