package resto.booking.core;

public abstract class BookingDecorator extends BookingComponent{
	public BookingComponent record;
		
	public BookingDecorator (BookingComponent record) {
		this.record = record;
	}

	public Party getPartyList() {
		return record.getPartyList();
	}
	public void setPartyList(Party partyList) {
		record.setPartyList(partyList);
	}

	public int getTableSize() {
		return record.getTableSize();
	}
	public void setTableSize(int tableSize) {
		record.setTableSize(tableSize);
	}

	public int getNumOfTable() {
		return record.getNumOfTable();
	}
	public void setNumOfTable(int numOfTable) {
		record.setNumOfTable(numOfTable);
	}

	public void book(String name, int partySize, LocalDateTime dateTime) {
		return record.book(name, partySize, dateTime);
	}

	public String getBookingList() {
		return record.getBookingList();
	}
}

