package resto.booking.core;

public abstract class BookingComponent implements Booking{
	protected Party partyList;
	protected int tableSize;
	protected int numOfTable;

	public BookingComponent() {

	} 

	public BookingComponent(Party partyList, int tableSize, int numOfTable) {
		this.partyList = partyList;
		this.tableSize = tableSize;
		this.numOfTable = numOfTable;
	}

	public Party getPartyList() {
		return this.partyList;
	}
	public void setPartyList(Party partyList) {
		this.partyList = partyList;
	}

	public int getTableSize() {
		return this.tableSize;
	}
	public void setTableSize(int tableSize) {
		this.tableSize = tableSize;
	}

	public int getNumOfTable() {
		return this.numOfTable;
	}
	public void setNumOfTable(int numOfTable) {
		this.numOfTable = numOfTable;
	}

 
	public abstract void book(String name, int partySize, LocalDateTime dateTime);

	public abstract String getBookingList();
}
