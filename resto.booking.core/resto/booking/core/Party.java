package resto.booking.core;

public abstract class Party extends Party{
	public Party record;
		
	public Party (Party record) {
		this.record = record;
	}

	public String getName() {
		return record.getName();
	}
	public void setName(String name) {
		record.setName(name);
	}

	public int getSize() {
		return record.getSize();
	}
	public void setSize(int size) {
		record.setSize(size);
	}

	public int getNumOfTable() {
		return record.getNumOfTable();
	}
	public void setNumOfTable(int numOfTable) {
		record.setNumOfTable(numOfTable);
	}

	public LocalDateTime getDateTime() {
		return record.getDateTime();
	}
	public void setDateTime(LocalDateTime dateTime) {
		record.setDateTime(dateTime);
	}

}

