package resto.promo.discount;

import resto.promo.core.PromoDecorator;
import resto.promo.core.Promo;
import resto.promo.core.PromoComponent;

public class PromoImpl extends PromoDecorator {

	protected double discountAmount;
	protected double minBuyAmount;
	
	public PromoImpl(PromoComponent record, double discountAmount, double minBuyAmount) {
		super(record);
		this.discountAmount = discountAmount;
		this.minBuyAmount = minBuyAmount;
	}

	public double getDiscountAmount() {
		return this.discountAmount;
	}

	public void setDiscountAmount(double discountAmount) {
		this.discountAmount = discountAmount;
	}
	public double getMinBuyAmount() {
		return this.minBuyAmount;
	}

	public void setMinBuyAmount(double minBuyAmount) {
		this.minBuyAmount = minBuyAmount;
	}

	public Boolean claim(String code) {
		throw new UnsupportedOperationException();
	}

	public String getInfo() {
		// TODO: implement this method
	}

	public Boolean claim(String code, double totalPrice) {
		// TODO: implement this method
	}
}
