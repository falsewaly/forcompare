package resto.menu.taxed;

import resto.menu.core.MenuDecorator;
import resto.menu.core.Menu;
import resto.menu.core.MenuComponent;

public class MenuImpl extends MenuDecorator {

	protected double tax;
	
	public MenuImpl(MenuComponent record, double tax) {
		super(record);
		this.tax = tax;
	}

	public double getTax() {
		return this.tax;
	}

	public void setTax(double tax) {
		this.tax = tax;
	}

	public String getInfo() {
		// TODO: implement this method
	}
}
