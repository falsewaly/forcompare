package resto.menu.core;

public abstract class MenuDecorator extends MenuComponent{
	public MenuComponent record;
		
	public MenuDecorator (MenuComponent record) {
		this.record = record;
	}

	public String getRestaurantName() {
		return record.getRestaurantName();
	}
	public void setRestaurantName(String restaurantName) {
		record.setRestaurantName(restaurantName);
	}

	public Item getItemList() {
		return record.getItemList();
	}
	public void setItemList(Item itemList) {
		record.setItemList(itemList);
	}

	public String getInfo() {
		return record.getInfo();
	}

	public void addItem() {
		return record.addItem();
	}
}

