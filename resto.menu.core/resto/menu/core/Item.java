package resto.menu.core;

public abstract class Item extends Item{
	public Item record;
		
	public Item (Item record) {
		this.record = record;
	}

	public String getName() {
		return record.getName();
	}
	public void setName(String name) {
		record.setName(name);
	}

	public double getPrice() {
		return record.getPrice();
	}
	public void setPrice(double price) {
		record.setPrice(price);
	}

}

