package resto.menu.core;

public interface Menu {
	public String getInfo();
	public void addItem();
}
