package resto.menu.core;

public abstract class MenuComponent implements Menu{
	protected String restaurantName;
	protected Item itemList;

	public MenuComponent() {

	} 

	public MenuComponent(String restaurantName, Item itemList) {
		this.restaurantName = restaurantName;
		this.itemList = itemList;
	}

	public String getRestaurantName() {
		return this.restaurantName;
	}
	public void setRestaurantName(String restaurantName) {
		this.restaurantName = restaurantName;
	}

	public Item getItemList() {
		return this.itemList;
	}
	public void setItemList(Item itemList) {
		this.itemList = itemList;
	}

 
	public abstract String getInfo();

	public abstract void addItem();
}
