module resto.promo.discountforspecificitem {
	exports resto.promo.discountforspecificitem;
	requires resto.promo.core;
	requires resto.promo.discount;
	requires resto.promo.specificitem;
}
