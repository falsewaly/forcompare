package resto.promo.specificitem;

import resto.promo.core.PromoDecorator;
import resto.promo.core.Promo;
import resto.promo.core.PromoComponent;

public class DSpecificItem extends PromoDecorator {

	protected String itemName;
	
	public DSpecificItem(PromoComponent record, String itemName) {
		super(record);
		this.itemName = itemName;
	}

	public String getItemName() {
		return this.itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public Boolean claim(String code, double totalPrice) {
		throw new UnsupportedOperationException();
	}

	public Boolean claim(String code, double totalPrice, String itemName) {
		// TODO: implement this method
	}
}
