package resto.menu.serviced;

import resto.menu.core.MenuDecorator;
import resto.menu.core.Menu;
import resto.menu.core.MenuComponent;

public class MenuImpl extends MenuDecorator {

	protected double service;
	
	public MenuImpl(MenuComponent record, double service) {
		super(record);
		this.service = service;
	}

	public double getService() {
		return this.service;
	}

	public void setService(double service) {
		this.service = service;
	}

	public String getInfo() {
		// TODO: implement this method
	}
}
