package resto.promo;

import resto.promo.core.Promo;
import java.lang.reflect.Constructor;
import java.util.logging.Logger;

public class PromoFactory{
    private static final Logger LOGGER = Logger.getLogger(PromoFactory.class.getName());

    public PromoFactory()
    {

    }

    public static Promo createPromo(String fullyQualifiedName, Object ... base)
    {
        Promo record = null;
        try {
            Class<?> clz = Class.forName(fullyQualifiedName);
            Constructor<?> constructor = clz.getDeclaredConstructors()[0];
            record = (Promo) constructor.newInstance(base);
        } 
        catch (IllegalArgumentException e)
        {
            LOGGER.severe("Failed to create instance of Promo.");
            LOGGER.severe("Given FQN: " + fullyQualifiedName);
            LOGGER.severe("Failed to run: Check your constructor argument");
            System.exit(20);
        }
        catch (ClassCastException e)
        {   LOGGER.severe("Failed to create instance of Promo.");
            LOGGER.severe("Given FQN: " + fullyQualifiedName);
            LOGGER.severe("Failed to cast the object");
            System.exit(30);
        }
        catch (ClassNotFoundException e)
        {
            LOGGER.severe("Failed to create instance of Promo.");
            LOGGER.severe("Given FQN: " + fullyQualifiedName);
            LOGGER.severe("Decorator can't be applied to the object");
            System.exit(40);
        }
        catch (Exception e)
        {
            LOGGER.severe("Failed to create instance of Promo.");
            LOGGER.severe("Given FQN: " + fullyQualifiedName);
            System.exit(50);
        }
        return record;
    }


    public static boolean checkConfig(String fqn, Object base)
    {
       boolean a = true;
       if (fqn.equals("resto.promo.discountforspecificitem.PromoImpl"))
        {
           String baseku = base.getClass().getCanonicalName();
           a = baseku.equals("resto.promo.discount.PromoImpl");
        }
        return a;
    }
}

