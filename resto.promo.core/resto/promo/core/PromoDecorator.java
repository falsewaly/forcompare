package resto.promo.core;

public abstract class PromoDecorator extends PromoComponent{
	public PromoComponent record;
		
	public PromoDecorator (PromoComponent record) {
		this.record = record;
	}

	public String getName() {
		return record.getName();
	}
	public void setName(String name) {
		record.setName(name);
	}

	public String getCode() {
		return record.getCode();
	}
	public void setCode(String code) {
		record.setCode(code);
	}

	public String getTnc() {
		return record.getTnc();
	}
	public void setTnc(String tnc) {
		record.setTnc(tnc);
	}

	public int getQuota() {
		return record.getQuota();
	}
	public void setQuota(int quota) {
		record.setQuota(quota);
	}

	public String getInfo() {
		return record.getInfo();
	}

	public Boolean claim(String code) {
		return record.claim(code);
	}
}

