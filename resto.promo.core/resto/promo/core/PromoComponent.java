package resto.promo.core;

public abstract class PromoComponent implements Promo{
	protected String name;
	protected String code;
	protected String tnc;
	protected int quota;

	public PromoComponent() {

	} 

	public PromoComponent(String name, String code, String tnc, int quota) {
		this.name = name;
		this.code = code;
		this.tnc = tnc;
		this.quota = quota;
	}

	public String getName() {
		return this.name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return this.code;
	}
	public void setCode(String code) {
		this.code = code;
	}

	public String getTnc() {
		return this.tnc;
	}
	public void setTnc(String tnc) {
		this.tnc = tnc;
	}

	public int getQuota() {
		return this.quota;
	}
	public void setQuota(int quota) {
		this.quota = quota;
	}

 
	public abstract String getInfo();

	public abstract Boolean claim(String code);
}
