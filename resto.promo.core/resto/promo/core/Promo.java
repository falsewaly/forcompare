package resto.promo.core;

public interface Promo {
	public Boolean claim(String code);
	public String getInfo();
}
