package resto.promo.cashback;

import resto.promo.core.PromoDecorator;
import resto.promo.core.Promo;
import resto.promo.core.PromoComponent;

public class PromoImpl extends PromoDecorator {

	protected double cashbackAmount;
	protected int cashbackLifetime;
	
	public PromoImpl(PromoComponent record, double cashbackAmount, int cashbackLifetime) {
		super(record);
		this.cashbackAmount = cashbackAmount;
		this.cashbackLifetime = cashbackLifetime;
	}

	public double getCashbackAmount() {
		return this.cashbackAmount;
	}

	public void setCashbackAmount(double cashbackAmount) {
		this.cashbackAmount = cashbackAmount;
	}
	public int getCashbackLifetime() {
		return this.cashbackLifetime;
	}

	public void setCashbackLifetime(int cashbackLifetime) {
		this.cashbackLifetime = cashbackLifetime;
	}

	public String getInfo() {
		// TODO: implement this method
	}
}
